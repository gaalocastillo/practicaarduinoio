#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <unistd.h>   // para usleep()
#include <getopt.h>
#include <math.h>


#include "arduino-serial-lib.h"

float calculateSD(float data[]);
float calculateMean(float data[]);
float calculateMin(float data[], int tam);
float calculateMax(float data[], int tam);



void error(char* msg)
{
    fprintf(stderr, "%s\n",msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	int fd = -1;
	int baudrate = 9600;  // default

	fd = serialport_init("/dev/ttyACM0", baudrate);

	if( fd==-1 )
	{
		error("couldn't open port");
		return -1;
	}
	else{

	int i;
	
	char opcion;
	char opcion_temp='t';
	char opcion_h='h';
	int temp=0;
	int hum=0;

	int temperaturaMaxima = 0;
	int temperaturaMinima = 0;
	int humedadMaxima = 0;
	int humedadMinima = 0;

	float mediaTemperatura = 0.0;
	float mediaHumedad = 0.0;
	float desvTemperatura = 0.0;
	float desvHumedad = 0.0;

	printf("Ingrese 0 para salir del sistema, o 1 para ingresar\n");
	scanf("%c",&opcion);
	while(opcion=='1'){
		serialport_flush(fd);
		int tam=0;
		printf("Ingrese el tamaño de la muestra\n");
		scanf("%d",&tam);
		int temperaturas[tam];
		int humedades[tam];
		for(i=0;i<tam;i++){
			write(fd, &opcion_temp, 1);
			usleep(5000);
            read(fd, &temp , 1);
            printf("Temperatura: %d \n",temp );
            temperaturas[i]=temp;
            usleep(5000);
            write(fd, &opcion_h, 1);
			usleep(5000);
            read(fd, &hum , 1);
            printf("Humedad: %d \n",temp );
            humedades[i]=hum;
            sleep(5);

		}

		printf("\tTEMPERATURA:\n");
        temperaturaMaxima = calculateMax(temperaturas, tam);
        printf("\nTemperatura maxima: %d ", temperaturaMaxima);
        temperaturaMinima = calculateMin(temperaturas, tam);
        printf("\nTemperatura minima: %d ", temperaturaMinima);
        mediaTemperatura = calculateMean(temperaturas);
        printf("\nValor medio de temperaturas: %f", mediaTemperatura);
        desvTemperatura = calculateSD(temperaturas);
        printf("\nDesviacion estandar de temperaturas: %f", desvTemperatura);

		printf("\tHUMEDAD:\n");
        humedadMaxima = calculateMax(humedades, tam);
        printf("\nHumedad maxima: %d ", humedadMaxima);
        humedadMinima = calculateMin(humedades, tam);
        printf("\nHumedad minima: %d ", humedadMinima);
        mediaHumedad = calculateMean(humedades);
        printf("\nValor medio de humedades: %f", mediaHumedad);
        desvHumedad = calculateSD(humedades);
        printf("\nDesviacion estandar de humedades: %f", desvHumedad);

		printf("\nIngrese 0 para salir del sistema, o 1 para ingresar\n");
		scanf("%c",&opcion);

	}

	close( fd );
	return 0;	
	}

}

/* Ejemplo para calcular desviacion estandar y media */
float calculateSD(float data[])
{
    float sum = 0.0, mean, standardDeviation = 0.0;

    int i;
    int tam=sizeof(data)/sizeof(data[0]);

    for(i = 0; i < tam; ++i)
    {
        sum += data[i];
    }

    mean = sum/tam;

    for(i = 0; i < tam; ++i)
        standardDeviation += pow(data[i] - mean, 2);

    return sqrt(standardDeviation /tam);
}

float calculateMean(float data[]){
    float sum=0.0;
    int tam=sizeof(data)/sizeof(data[0]);
    for(int i=0;i<tam;++i){
        sum+=data[i];
        
    }
    return sum/tam;
}

float calculateMax(float data[], int tam){
	float maximo;
	maximo = data[0];
	for(int i=0;i<tam;++i){
        if (maximo < data[i])
        {
        	maximo = data[0];
        }   
    }

    return maximo;
}

float calculateMin(float data[], int tam){
	float minimo;
	minimo = data[0];
	for(int i=0;i<tam;++i){
        if (minimo > data[i])
        {
        	minimo = data[0];
        }   
    }
    return minimo;
}

